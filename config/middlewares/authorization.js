
/*
 *  Generic require login routing middleware
 */

exports.requiresLogin = function (req, res, next) {
  if (!req.isAuthenticated()) {
    req.session.returnTo = req.originalUrl
    return res.redirect('/login')
  }
  next()
}

/*
 *  User authorization routing middleware
 */

exports.user = {
  hasAuthorization : function (req, res, next) {
    if (req.profile.id != req.user.id) {
      req.flash('info', 'Вы не авторизированы')
      return res.redirect('/users/'+req.profile.id)
    }
    next()
  }
}

exports.user = {
    hasAdmin : function (req, res, next) {
        if (!req.isAuthenticated()) {
            req.session.returnTo = req.originalUrl
            req.flash('error', 'Вы не авторизированы')
            return res.redirect('/')
        }

        if (req.user.role!='admin') {
            req.flash('info', 'Вы не админ')
            return res.redirect('/')
        }
        next()
    }
}

/*
 *  Room authorization routing middleware
 */

exports.room = {
  hasAuthorization : function (req, res, next) {
    if (req.user.role!='admin'){
    if (req.room.user.id != req.user.id) {
      req.flash('info', 'Вы не авторизированы')
      return res.redirect('/rooms/'+req.room.id)
    }
    }
    next()
  }
}
