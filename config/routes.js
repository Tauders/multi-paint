/*!
 * Module dependencies.
 */

var async = require('async')

/**
 * Controllers
 */

var users = require('../app/controllers/users')
  , rooms = require('../app/controllers/rooms')
  , auth = require('./middlewares/authorization')
  , other = require('../app/controllers/other')

/**
 * Route middlewares
 */

var roomAuth = [auth.requiresLogin, auth.room.hasAuthorization]

/**
 * Expose routes
 */

module.exports = function (app, passport) {

  // user routes
  app.get('/login', users.login)
  app.get('/signup', users.signup)
  app.get('/logout', users.logout)
  app.post('/users', users.create)
  app.get('/export-users', auth.user.hasAdmin, users.exportUsers)
  app.post('/users/session',
    passport.authenticate('local', {
      failureRedirect: '/login',
      failureFlash: 'Неверный логин или пароль.'
    }), users.session)
  app.get('/users/:userId', users.show)


  app.param('userId', users.user)

  // room routes
  app.get('/rooms', rooms.index)
  app.get('/rooms/new', auth.requiresLogin, rooms.new)
  app.post('/rooms', auth.requiresLogin, rooms.create)
  app.get('/rooms/:id',auth.requiresLogin, rooms.show)
  app.get('/rooms/:id/edit', roomAuth, rooms.edit)
  app.put('/rooms/:id', roomAuth, rooms.update)
  app.del('/rooms/:id', roomAuth, rooms.destroy)

  app.param('id', rooms.load)

  // home route
  app.get('/', other.index)


}
