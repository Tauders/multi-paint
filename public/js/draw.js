$(function(){
    var sizeslider=$( "#slider-brush"),
        drawmod = $('#drawmod'),
        socket = io.connect(),
        tmparr = [],
        title = window.location.href.split('/')[4];

    function SendToSocketCanvasChange(modif){
        socket.emit('canvas-change',{title:title, data:JSON.stringify(tmparr)},modif);
    }

    var canvas = new fabric.Canvas('canvasMain', {
        isDrawingMode: true
    });

    canvas.setDimensions({width:1050,height:600});

    $("#PencilBrush, #CircleBrush").click(function(e){
        e.preventDefault();
        canvas.isDrawingMode = true;
        drawmod.html('Выключить режим рисования');
        canvas.freeDrawingBrush = new fabric[e.currentTarget.id](canvas);
        canvas.freeDrawingBrush.width = parseInt(sizeslider.slider( "value" ), 10) || 1;
        canvas.freeDrawingBrush.color =  hexformat('brush');
    });

    drawmod.click( function() {
        canvas.isDrawingMode = !canvas.isDrawingMode;

        if (canvas.isDrawingMode) {
            drawmod.html('Выключить режим рисования');

        }
        else {
            drawmod.html('Включить режим рисования');

        }
    });

    $('#download_jpg').click(function(e){
        e.preventDefault();
        canvas.deactivateAllWithDispatch();
        window.open(canvas.toDataURL({format: 'png'}));
    });

    $("#Rect, #Circle, #Triangle").click(function(e){
        canvas.isDrawingMode = false;
        drawmod.html('Включить режим рисования');
        var options = {
            top: 200,
            left: 150
        };
        if( $('#fill-or-not').is(':checked') ){
            options.fill=hexformat('fill');
        }
        else{
            options.fill=null;
            options.stroke= hexformat('brush');
            options.strokeWidth= parseInt(sizeslider.slider( "value" ), 10) || 1;
        }
        if ([e.currentTarget.id] == 'Circle') {
            options.radius = 50 * Math.random()+50;
        }
        else {
            options.width = 150;
            options.height = 120;
        }
        var figure = new fabric[e.currentTarget.id](options);
        canvas.add(figure).setActiveObject(figure);

        tmparr[0] = canvas.getObjects()[canvas.getObjects().length-1];
        canvas.getObjects()[canvas.getObjects().length-1].setId(guid());
        SendToSocketCanvasChange('add');

    });

    $('#btn_clear').click(function(){
        canvas.clear();
        socket.emit('clear',{title:title});
    });

    socket.on('clear', function(){
        canvas.clear();
    });

    socket.emit('req_data',{title:title});

    socket.on('action', function (data, metr) {
        fabric.util.enlivenObjects(JSON.parse(data), function(objects) {
            var origRenderOnAddRemove = canvas.renderOnAddRemove;
            canvas.renderOnAddRemove = false;

            switch(metr){
                case 'add':
                    canvas.add(objects[0]);
                    break;
                case 'mod':
                    canvas.deactivateAllWithDispatch();
                    canvas.getObjects().forEach(function(o){
                        if (o.getId()==objects[0].getId())
                        {
                            canvas.remove(o);
                        }
                    });
                    canvas.add(objects[0]);
                    break;
                case 'load':
                    objects.forEach(function(o){
                        canvas.add(o);
                    });
                    break;
                case 'del':
                    canvas.getObjects().forEach(function(o){
                        if (o.getId()==objects[0].getId())
                        {
                            canvas.remove(o);
                        }
                    });
                    break;
                default:
                    break;
            }

            canvas.renderOnAddRemove = origRenderOnAddRemove;
            canvas.renderAll();
        });

    });

    $('#btn_del_obj').click( function (o){
        deletefig();
    });

    function deletefig(){
        var text = canvas.getActiveObject();
        var test = canvas.getActiveGroup();
        if (text || test){
            canvas.deactivateAllWithDispatch();
            if (test==null){
                tmparr[0] = text;
                canvas.remove(text);
                SendToSocketCanvasChange('del');
            }
            else{
                test._objects.forEach(function (o){
                    tmparr[0] = o;
                    SendToSocketCanvasChange('del');
                    canvas.remove(o);
                });

            }
        }
    }

    canvas.on('object:moving', function(option){
        var target  = option.target;
        if (target.top<0 || target.top>canvas.height || target.left<0 || target.left>canvas.width){
            target.left=neededoffsetleft(target,0,'less');
            target.left=neededoffsetleft(target,canvas.width,'more');
            target.top = neededoffsettop(target,0,'less');
            target.top = neededoffsettop(target,canvas.height,'more');
        }
    });

    canvas.on('object:modified', function(option){
        if ( typeof (option.target._objects)=='object' && option.target.getId()==null) {

            option.target._objects.forEach(function(o){
                changecoords(o,'add');
                tmparr[0] = o;
                SendToSocketCanvasChange('mod');
                changecoords(o,'minus');
            });
        }
        else{
        tmparr[0] = option.target;
        //console.log(tmparr[0]);
            SendToSocketCanvasChange('mod');
        //socket.emit('canvas-change',{title:title, data:JSON.stringify(tmparr)}, 'mod');
        }
    });

    $(document).keydown(function(e){
        if (e.which==46){
            deletefig();
        }
    });

    canvas.on('mouse:up', function(options){

        if (canvas.isDrawingMode==true)
        {
            canvas.getObjects()[canvas.getObjects().length-1].setId(guid());
            tmparr[0] = canvas.getObjects()[canvas.getObjects().length-1];
            SendToSocketCanvasChange('add');
        }

    });

    sizeslider.slider({
        orientation: "horizontal",
        range: "min",
        min: 1,
        max: 50,
        value: 5,
        slide: function( event, ui ) {
            canvas.freeDrawingBrush.width = parseInt(ui.value, 10) || 1;
            $( "#badge_size" ).html( ui.value );
        }
    });

    function refreshSwatch(type) {
        canvas.freeDrawingBrush.color = hexformat(type);
        $( '#swatch-'+type ).css( "background-color", hexformat(type));
    }

    $('#imagefile').on('change', function(e){
        var file = e.originalEvent.target.files[0],
            reader = new FileReader();
        reader.onload = function(evt){
            fabric.Image.fromURL(evt.target.result, function(img) {
                canvas.isDrawingMode=false;
                var oImg = img.set({ left: 300, top: 300});
                canvas.add(oImg).renderAll();
                canvas.setActiveObject(oImg);
                drawmod.html('Включить режим рисования');
                tmparr[0] = canvas.getObjects()[canvas.getObjects().length-1];
                canvas.getObjects()[canvas.getObjects().length-1].setId(guid());
                SendToSocketCanvasChange('add');
            });

        };
        reader.readAsDataURL(file);
    });

    $( "#red-brush, #red-fill, #green-fill, #green-brush, #blue-brush, #blue-fill " ).slider({
        orientation: "horizontal",
        range: "min",
        max: 255,
        value: 127,
        slide: function(event,ui){
            var type = ui.handle.parentElement.id.split('-')[1];
            refreshSwatch(type);
        },
        change: function(event,ui){
            var type = ui.handle.parentElement.id.split('-')[1];
            refreshSwatch(type);
        }
    });

    $( "#red-brush" ).slider( "value", 256 * Math.random() );
    $( "#green-brush" ).slider( "value", 256 * Math.random() );
    $( "#blue-brush" ).slider( "value", 256 * Math.random() );

    $( "#red-fill" ).slider( "value", 256 * Math.random() );
    $( "#green-fill" ).slider( "value", 256 * Math.random() );
    $( "#blue-fill" ).slider( "value", 256 * Math.random() );

    if (canvas.freeDrawingBrush) {
        canvas.freeDrawingBrush.width = parseInt(sizeslider.slider( "value" ), 10) || 1;
        canvas.freeDrawingBrush.color =  hexformat('brush');
        $( "#badge_size" ).html( sizeslider.slider( "value" ) );
    }

    window.onbeforeunload = function(e){
        socket.emit('add-image',{title:title,data:canvas.toDataURL({format: 'png'})});
    };
});