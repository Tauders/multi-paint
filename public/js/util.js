function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
};

function guid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

function hexFromRGB(r, g, b) {
    var hex = [
        r.toString( 16 ),
        g.toString( 16 ),
        b.toString( 16 )
    ];
    $.each( hex, function( nr, val ) {
        if ( val.length === 1 ) {
            hex[ nr ] = "0" + val;
        }
    });
    return hex.join( "" ).toUpperCase();
}

function hexformat(tmp) {
    var red = $( '#red-'+ tmp ).slider( "value" ),
        green = $( '#green-'+ tmp ).slider( "value" ),
        blue = $( '#blue-'+ tmp ).slider( "value" );
    hex = hexFromRGB( red, green, blue );
    return "#" + hex;
}

function moreorless(target,val,met){
    switch(met){
        case 'less':
            if (target<val){
                target=val;
            }
            break;
        case 'more':
            if (target>val){
                target=val;
            }
            break;
        default:
            break;
    }
    return target;
}

function neededoffsettop(target,val,met){
    var tmp = target.top;
    tmp = moreorless(tmp,val,met);

    return tmp;
}
function neededoffsetleft(target,val,met){
    var tmp = target.left;
    tmp = moreorless(tmp,val,met);

    return tmp;
}

function changecoords(target,sign){
    if (sign=='add'){
        target.left = target.left+target.group.left;
        target.top = target.top+target.group.top;
    }
    else{
        target.left = target.left - target.group.left;
        target.top = target.top - target.group.top;
    }
}
