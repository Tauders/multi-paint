
/*!
 * nodejs-express-mongoose-demo
 * Copyright(c) 2013 Madhusudhan Srinivasa <madhums8@gmail.com>
 * MIT Licensed
 */

/**
 * Module dependencies.
 */

var express = require('express')
  , fs = require('fs')
  , passport = require('passport')
  , http = require('http')

/**
 * Main application entry file.
 * Please note that the order of loading is important.
 */

// Load configurations
// if test env, load example file
var env = process.env.NODE_ENV || 'development'
  , config = require('./config/config')[env]
  , mongoose = require('mongoose')

// Bootstrap db connection
mongoose.connect(config.db)

// Bootstrap models
var models_path = __dirname + '/app/models'
fs.readdirSync(models_path).forEach(function (file) {
  if (~file.indexOf('.js')) require(models_path + '/' + file)
})

// bootstrap passport config
require('./config/passport')(passport, config)

var app = express()
// express settings
require('./config/express')(app, config, passport)


// Bootstrap routes
require('./config/routes')(app, passport)

// Start the app by listening on <port>
var port = process.env.PORT || 3000
//app.listen(port)

var server = http.createServer(app);
var io = require('socket.io').listen(server);
server.listen(port);
io.set('log level', 1);
console.log('Express app started on port '+port)

// expose app
exports = module.exports = app

Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

io.sockets.on('connection', function (socket) {

    socket.on('clear', function(data){
        var Room = mongoose.model('Room');
        Room.findOne({_id:data.title},function(err,room){
//            room.body.length = 0;
            room.clearElements();
            socket.broadcast.emit('clear');
        });

        //arr.length = 0;
    });

    socket.on('canvas-change', function (data, metr) {
        //console.log(data.title);
        var Room = mongoose.model('Room');
        var tmp = JSON.parse(data.data);

        Room.findOne({_id:data.title},function(err,room){
            var db = room.body;

            switch(metr){
                case 'add':
                    db.push(JSON.parse(data.data)[0]);
                    break;
                case 'mod':
                    db.forEach(function logArrayElements(element, index, array) {
                        if (tmp[0].idfig==element.idfig)
                        {
                            db.remove(element);
                        }
                    });
                    db.push(tmp[0]);
                    break;
                case 'del':
                    db.forEach(function logArrayElements(element, index, array) {
                        if (tmp[0].idfig==element.idfig)
                        {
                            db.remove(element);
                        }
                    });
                    break;
                default:
                    break;
            }

            room.addElement(db[0]);

            socket.broadcast.emit('action', data.data, metr);

        });

    });

    socket.on('req_data', function(data){
        var Room = mongoose.model('Room');

        Room.findOne({_id:data.title},function(err,room){
            socket.emit('action', JSON.stringify(room.body), 'load');
        });
    });

    socket.on('add-image',function(data){
        var Room = mongoose.model('Room');

        Room.findOne({_id:data.title},function(err,room){
            room.addImage(data.data);
        });
    });

});