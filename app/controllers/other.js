
/**
 * Module dependencies.
 */

var mongoose = require('mongoose')
    , User = mongoose.model('User')

/**
 * Index
 */

exports.index = function(req, res){
    if (req.isAuthenticated()) {
        return res.redirect('/rooms')
    }

    res.render('other/index', {
        title: 'Главная страница',
        user: new User(),
        message: req.flash('error')
    })
}