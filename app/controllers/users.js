
/**
 * Module dependencies.
 */

var mongoose = require('mongoose')
  , User = mongoose.model('User')
  , utils = require('../../lib/utils')
  , jsonxml = require('jsontoxml')

var login = function (req, res) {
  if (req.session.returnTo) {
    res.redirect(req.session.returnTo)
    delete req.session.returnTo
    return
  }
  res.redirect('/')
}

exports.signin = function (req, res) {}

/**
 * Auth callback
 */

exports.authCallback = login

/**
 * Show login form
 */

exports.login = function (req, res) {
  res.render('users/login', {
    title: 'Войти',
    message: req.flash('error')
  })
}

/**
 * Show sign up form
 */

exports.signup = function (req, res) {

  res.render('users/signup', {
    title: 'Зарегистрироваться',
    user: new User()
  })
}

/**
 * Logout
 */

exports.logout = function (req, res) {
  req.logout()
  res.redirect('/')
}

/**
 * Session
 */

exports.session = login

/**
 * Create user
 */

exports.create = function (req, res) {
  var user = new User(req.body)
  user.provider = 'local'

  if (user.username=='admin'){user.role='admin'}

  user.save(function (err) {
    if (err) {
      return res.render('users/signup', {
        errors: utils.errors(err.errors),
        user: user,
        title: 'Зарегистрироваться'
      })
    }

    // manually login the user once successfully signed up
    req.logIn(user, function(err) {
      if (err) return next(err)
      return res.redirect('/')
    })
  })
}

/**
 *  Show profile
 */

exports.show = function (req, res) {
  var user = req.profile
  res.render('users/show', {
    title: user.username,
    user: user
  })
}

/**
 * Export all users to xml
 */

exports.exportUsers = function (req, res) {
    User.find({}, function (err, users) {
        var json = JSON.stringify(users);
        var xml = jsonxml(json);

        res.header("Content-Type", "text/xml");
        res.header("Content-Length", xml.length);
        var fileName = "users.xml";
        res.header("Content-disposition", "attachment; filename=" + fileName);

        res.send(xml);
    });

    var xml = "";
    res.send("");
}

/**
 * Find user by id
 */

exports.user = function (req, res, next, id) {
  User
    .findOne({ _id : id })
    .exec(function (err, user) {
      if (err) return next(err)
      if (!user) return next(new Error('Не удалось загрузить пользователя ' + id))
      req.profile = user
      next()
    })
}
