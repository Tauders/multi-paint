
/**
 * Module dependencies.
 */

var mongoose = require('mongoose')
  , Room = mongoose.model('Room')
  , utils = require('../../lib/utils')
  , _ = require('underscore')
  , User = mongoose.model('User')

/**
 * Load
 */

exports.load = function(req, res, next, id){
    var User = mongoose.model('User')

  Room.load(id, function (err, room) {
    if (err) return next(err)
    if (!room) return next(new Error('not found'))
    req.room = room
    next()
  })
}

/**
 * List
 */

exports.index = function(req, res){
  if (!req.isAuthenticated()) {
      return res.redirect('/')
  }
  var options = {

  }

  Room.list(options,function(err, rooms) {
    if (err) return res.render('500')
    Room.count().exec(function (err, count) {
      res.render('rooms/index', {
        title: 'Список комнат',
        user: req.user,
        rooms: rooms
      })
    })
  })  
}


/**
 * New room
 */

exports.new = function(req, res){
  res.render('rooms/new', {
    title: 'Новая комната',
    room: new Room({})
  })
}

/**
 * Create an room
 */

exports.create = function (req, res) {
  var room = new Room(req.body)
  room.user = req.user


  room.uploadAndSave(function (err) {
    if (!err) {
      req.flash('success', 'Комната успешно создана!')
      return res.redirect('/rooms/'+room._id)
    }

    res.render('rooms/new', {
      title: 'Новая комната',
      room: room,
      errors: utils.errors(err.errors || err)
    })
  })
}

/**
 * Edit an room
 */

exports.edit = function (req, res) {
  res.render('rooms/edit', {
    title: 'Изменить ' + req.room.title,
    room: req.room
  })
}

/**
 * Update room
 */

exports.update = function(req, res){
  var room = req.room
  room = _.extend(room, req.body)

  room.uploadAndSave(function(err) {
    if (!err) {
      return res.redirect('/rooms/' + room._id)
    }

    res.render('rooms/edit', {
      title: 'Изменить комнату',
      room: room,
      errors: err.errors
    })
  })
}

/**
 * Show
 */

exports.show = function(req, res){
  res.render('rooms/show', {
    title: req.room.title,
    room: req.room
  })
}



/**
 * Delete an room
 */

exports.destroy = function(req, res){
  var room = req.room

  room.remove(function(err){
    req.flash('info', 'Успешно удалено')
    res.redirect('/rooms')
  })
}
