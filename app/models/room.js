
/**
 * Module dependencies.
 */

var mongoose = require('mongoose')
  , env = process.env.NODE_ENV || 'development'
  , Schema = mongoose.Schema

/**
 * Getters
 */

/**
 * Setters
 */

/**
 * Room Schema
 */

var RoomSchema = new Schema({
  title: {type : String, default : '', trim : true},
  body: {type : []},
  user: {type : Schema.ObjectId, ref : 'User'},
  image: {type: String},
  createdAt  : {type : Date, default : Date.now},
  type: {type:String,default:'off'}
})

/**
 * Validations
 */

RoomSchema.path('title').validate(function (title) {
    return title.length > 0
}, 'Название комнаты не может быть пустым')

RoomSchema.path('title').validate(function (title) {
    return /^[a-zA-Z0-9_]{1,15}$/.test(title)
}, 'Название комнаты должно быть от 1 до 15 символов, и содержать только английские буквы и цифры')


/**
 * Methods
 */

RoomSchema.methods = {

  /**
   * Save room
   *
   * @param {Function} cb
   * @api private
   */

  uploadAndSave: function (cb) {
    return this.save(cb)
  },

  addImage: function(str,cb){
    this.image = str;
    this.save(cb);
  },

  /**
   * Add element
   *
   * @param {User} obj
   * @param {Function} cb
   * @api private
   */

  addElement: function (obj, cb) {

    this.body.push(obj);
    this.save(cb)
  },

  clearElements: function (cb) {
    while (this.body.length > 0) {
          this.body.pop();
      }
    this.save(cb)
  }

}

/**
 * Statics
 */

RoomSchema.statics = {

  /**
   * Find room by id
   *
   * @param {ObjectId} id
   * @param {Function} cb
   * @api private
   */

  load: function (id, cb) {
    this.findOne({ _id : id })
      .populate('user', 'email username')
      .exec(cb)
  },

  /**
   * List rooms
   *
   * @param {Object} options
   * @param {Function} cb
   * @api private
   */

  list: function (options, cb) {
      var criteria = options.criteria || {};

    this.find(criteria)
      .populate('user', 'username')
      .sort({'createdAt': -1}) // sort by date
      .exec(cb)
  }

}

mongoose.model('Room', RoomSchema);
